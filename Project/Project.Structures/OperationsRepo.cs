﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Core.Operations;

namespace Project.Core
{
    public class OperationsRepo
    {
        private List<Operation> operations { get; set; }

        public OperationsRepo()
        {
            operations = new List<Operation>()
            {
                new Addition(),
                new Cosinus(),
                new Division(),
                new Exponentiation(),
                new Multiplication(),
                new NaturalLogarithm(),
                new Sinus(),
                new Sqrt(),
                new Subtraction()
            };
        }

        public List<Operation> GetOperations()
        {
            return operations.ToList();
        }
    }
}
