﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core
{
    public abstract class FileWorker
    {
        protected abstract DataPath Path { get; set; }
        public const string PathExtension = ".csv";
        protected string FullPath { get; set; }

        public void ChangePath(DataPath path)
        {
            Path = path;
            FullPath = Path + PathExtension;

            if (!File.Exists(FullPath))
                File.Create(FullPath);
        }
    }
}
