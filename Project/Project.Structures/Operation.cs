﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core
{
    public abstract class Operation : IComparable<Operation>
    {
        public abstract string Name { get; }
        public abstract int Priority { get; }
        public abstract bool hasTwoOperands { get; }
        public abstract double Calculate(double left, double? right = null);

        /// <returns>Если операндов 2 - true, иначе - false</returns>
        public bool Check(double left, double? right = null)
        {
            return right is not null;
        }

        public int CompareTo(Operation? other)
        {
            if(other is null) throw new ArgumentNullException(nameof(other));

            return Priority.CompareTo(other.Priority);
        }
    }
}
