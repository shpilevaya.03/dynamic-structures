﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core
{
    public class FileWriter : FileWorker
    {
        protected override DataPath Path { get; set; }

        public FileWriter(DataPath? path = DataPath.commands)
        {
            if (path == null) return;
            ChangePath((DataPath)path);
        }

        public void GenerateFile(string data)
        {
            File.WriteAllText(FullPath, data);
        }

        public void GenerateInfixFile(int count)
        {
            GenerateFile(DataWorker.GenerateInfix(count, new OperationsRepo()));
        }
    }
}
