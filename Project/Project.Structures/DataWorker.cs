﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Core.Structures;

namespace Project.Core
{
    public static class DataWorker
    {
        static readonly Random _rand = new Random();

        /// <summary>Генерация команд</summary>
        /// <param name="commands">Количество команд</param>
        /// <param name="count">Количество команд для генерации</param>

        public static string Generate(int commands, int count)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < count; i++)
            {
                int command = _rand.Next(1, commands + 1);
                if (command == 1) sb.Append($"{command},{_rand.Next()} ");
                else sb.Append(command + " ");
            }

            return sb.ToString();
        }

        /// <summary>Генерирует инфиксную запись</summary>
        /// <param name="count">Количество операций</param>
        public static string GenerateInfix(int count, OperationsRepo operationsRepo)
        {
            StringBuilder sb = new StringBuilder();
            List<Operation> operations = operationsRepo.GetOperations();
            bool mayBeOneOperand = true;
            int countBrackets = 0;

            for (int i = 0; i < count; i++)
            {
                Operation op = operations[_rand.Next(operations.Count())];
                bool mayBeBracket = _rand.Next(5) == 0;
                double num = _rand.NextDouble() * _rand.Next(1, 100) + 0.1;

                if (!op.hasTwoOperands && mayBeOneOperand || op.hasTwoOperands && !mayBeOneOperand)
                {
                    if (mayBeBracket) sb.Append($"{op.Name} ( {num} ");
                    else if (countBrackets > 0) sb.Append($"{op.Name} {num} ) ");
                    else sb.Append($"{op.Name} {num} ");
                }
                else if (mayBeOneOperand && op.hasTwoOperands)
                {
                    double num2 = _rand.NextDouble() + 0.1;
                    if (mayBeBracket) sb.Append($"( {num2} {op.Name} {num} ");
                    else if (countBrackets > 0) sb.Append($"{num2} {op.Name} {num} ) ");
                    else sb.Append($"{num2} {op.Name} {num} ");
                }
                else
                {
                    i++;
                    var name = op.Name;
                    while (!op.hasTwoOperands) op = operations[_rand.Next(operations.Count())];
                    if (mayBeBracket) sb.Append($"{op.Name} ( {name} {num} ");
                    else if (countBrackets > 0) sb.Append($"{op.Name} {name} {num} ) ");
                    else sb.Append($"{op.Name} {name} {num} ");
                }

                if(mayBeBracket) countBrackets++;
                else if(countBrackets > 0) countBrackets--;
                mayBeOneOperand = false;
            }

            while(countBrackets > 0)
            {
                sb.Append(") ");
                countBrackets--;
            }
            
            return sb.ToString();
        }

        public static string InfixToPostfix(string[] strSplit, OperationsRepo operationsRepo)
        {
            StackStruct<string> stack = new StackStruct<string>();
            List<Operation> operations = operationsRepo.GetOperations();

            StringBuilder sb = new StringBuilder();

            double num = 0;
            for (int i = 0; i < strSplit.Length; i++)
            {
                if (double.TryParse(strSplit[i], out num))
                {
                    sb.Append($"{strSplit[i]} ");
                }
                else if (strSplit[i].Equals("("))
                    stack.Push(strSplit[i]);
                else if (strSplit[i].Equals(")"))
                {
                    while (stack.Count() != 0 && !stack.Top().Equals("("))
                        sb.Append($"{stack.Pop()} ");
                    stack.Pop();
                }
                else
                {
                    Operation op = operations.FirstOrDefault(x => x.Name == strSplit[i]);
                    if (op == null) continue;

                    Operation top = operations.FirstOrDefault(x => stack.Count() != 0 && x.Name == stack.Top());
                    while (stack.Count() != 0 && top is not null && top.Priority >= op.Priority)
                    {
                        sb.Append($"{stack.Pop()} ");
                        top = operations.FirstOrDefault(x => stack.Count() != 0 && x.Name == stack.Top());
                    }
                    stack.Push(op.Name);
                }
            }

            while (stack.Count() != 0)
            {
                sb.Append($"{stack.Pop()} ");
            }

            return sb.ToString();
        }
    }
}