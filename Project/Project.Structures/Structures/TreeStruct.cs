﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Structures
{
    public class TreeStruct
    {
        public int Value;
        public int Count;
        public TreeStruct Left;
        public TreeStruct Right;

        public void Insert(int value)
        {
            if (this.Value == null) this.Value = value;
            else
            {
                if (this.Value.CompareTo(value) == 1)
                {
                    if (Left == null) this.Left = new TreeStruct();
                    Left.Insert(value);
                }
                else if (this.Value.CompareTo(value) == -1)
                {
                    if (Right == null) this.Right = new TreeStruct();
                    Right.Insert(value);
                }
                else throw new Exception("Node doesn't exist");
            }
            this.Count = Recount(this);
        }

        public TreeStruct Search(int value)
        {
            if (this.Value == value) return this;
            else if (this.Value.CompareTo(value) == 1)
            {
                if (Left != null) return this.Left.Search(value);
                else throw new Exception("Node doesn't exist");
            }
            else
            {
                if (Right != null) return this.Right.Search(value);
                else throw new Exception("Node doesn't exist");
            }
        }

        public string Display(TreeStruct tree)
        {
            string result = "";
            if (tree.Left != null) result += Display(tree.Left);

            result += tree.Value + " ";

            if (tree.Right != null) result += Display(tree.Right);

            return result;
        }

        private int Recount(TreeStruct tree)
        {
            int count = 0;
            if (tree.Left != null) count += Recount(tree.Left);
            count++;

            if (tree.Right != null) count += Recount(tree.Right);
            return count;
        }

        public void Clear()
        {
            this.Value = 0;
            this.Left = null;
            this.Right = null;
        }

        public bool IsEmpty() => this.Value == null ? true : false;
    }
}