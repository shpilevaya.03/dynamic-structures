﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core
{
    public class FileReader : FileWorker
    {
        protected override DataPath Path { get; set; }

        public FileReader(DataPath? path = DataPath.commands)
        {
            if (path == null) return;
            ChangePath((DataPath)path);
        }

        /// <summary>Чтение команд из файла</summary>
        /// <returns>массив команд</returns>
        public string[] ReadFile()
        {
            var str = File.ReadAllText(FullPath);
            return str.Split(' ', StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
