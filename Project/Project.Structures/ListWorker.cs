﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Core.Structures;

namespace Project.Core
{
    public static class ListWorker
    {
        /// <summary>
        /// 1.	Написать функцию, которая переворачивает список L, т.е. изменяет ссылки в этом списке так,
        /// чтобы его элементы оказались расположенными в обратном порядке. 
        /// </summary>

        public static void Reverse<T>(this LinkedListStruct<T> linkedList) where T : IComparable<T>
        {
            int linkedListLength = linkedList.Count();
            for (int i = linkedListLength - 1; i >= 0; i--)
            {
                linkedList.Add(linkedList.ElementAt(i));
                linkedList.RemoveAt(i);
            }
        }

        /// <summary>
        /// 2.	Написать функцию, которая переносит в начало (в конец)
        /// непустого списка L его последний (первый) элемент. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="isBegin">true, если перенос в начало, false - в конец</param>

        public static void Flip<T>(this LinkedListStruct<T> linkedList, bool isBegin) where T : IComparable<T>
        {
            int linkedListLength = linkedList.Count();
            if (isBegin)
            {
                linkedList.Insert(0, linkedList.ElementAt(linkedListLength - 1));
                linkedList.RemoveAt(linkedListLength);
            }
            else
            {
                linkedList.Add(linkedList.ElementAt(0));
                linkedList.RemoveAt(0);
            }
        }

        /// <summary>
        /// 3.	Написать функцию, которая определяет количество различных элементов списка,
        /// содержащего целые числа.
        /// </summary>

        public static int CountDistinct<T>(this LinkedListStruct<T> linkedList) where T : IComparable<T>
        {
            int linkedListLength = linkedList.Count();
            Dictionary<T, int> map = new Dictionary<T, int>();
            for (int i = 0; i < linkedListLength; i++)
            {
                map.TryAdd(linkedList.ElementAt(i), 1);
            }
            return map.Count;
        }

        /// <summary>
        /// 4.	Написать функцию, которая удаляет из списка L второй из двух одинаковых элементов.
        /// </summary>

        public static void RemoveSecondClone<T>(this LinkedListStruct<T> linkedList) where T : IComparable<T>
        {
            int linkedListLength = linkedList.Count();
            Dictionary<T, int> map = new Dictionary<T, int>();
            for (int i = 0; i < linkedListLength; i++)
            {
                map.TryAdd(linkedList.ElementAt(i), 1);
            }
            linkedList.Clear();
            foreach (var item in map)
            {
                linkedList.Add(item.Key);
            }
        }

        /// <summary>
        /// 5.	Написать функцию вставки списка самого в себя вслед за первым вхождением числа х.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="elem">Куда вставлять</param>

        public static void InsertInto<T>(this LinkedListStruct<T> linkedList, T elem) where T : IComparable<T>
        {
            int linkedListLength = linkedList.Count();
            LinkedListStruct<T> saved = new LinkedListStruct<T>();
            for (int e = 0; e < linkedListLength; e++)
                saved.Add(linkedList.ElementAt(e));

            for (int i = 0; i < linkedListLength; i++)
            {
                if (linkedList.ElementAt(i).Equals(elem))
                {
                    int postIndex = 1;
                    for (int j = 0; j < linkedListLength; j++)
                    {
                        linkedList.Insert(i + postIndex, saved.ElementAt(j));
                        postIndex++;
                    }
                    return;
                }
            }
            Console.WriteLine("This element doesn't exist");
        }

        /// <summary>
        /// 6.	Написать функцию, которая вставляет в непустой список L, 
        /// элементы которого упорядочены по не убыванию, новый элемент Е так, 
        /// чтобы сохранилась упорядоченность.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="linkedList"></param>

        public static void OrderedInsertion<T>(this LinkedListStruct<T> linkedList, T elem) where T : IComparable<T>
        {
            linkedList.Add(elem);
            linkedList.Sort();
        }

        /// <summary>
        /// 7. Написать функцию, которая удаляет из списка L все элементы Е, если таковые имеются.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="elem"></param>

        public static void RemoveAll<T>(this LinkedListStruct<T> linkedList, T elem) where T : IComparable<T>
        {
            int count = 0;

            while (count < linkedList.Count())
            {
                if (linkedList.ElementAt(count).Equals(elem))
                {
                    linkedList.RemoveAt(count);
                    count--;
                }
                count++;
            }
        }

        /// <summary>
        /// 8. Написать функцию, которая вставляет в список L новый элемент F
        /// перед первым вхождением элемента Е, если Е входит в L.
        /// </summary>
        /// <param name="newElem">Элемент для вставки</param>
        /// <param name="desiredElem">Элемент для ориентирования</param>

        public static void InsertInto<T>(this LinkedListStruct<T> linkedList, T newElem, T desiredElem) where T : IComparable<T>
        {
            for (int i = 0; i < linkedList.Count(); i++)
            {
                if (linkedList.ElementAt(i).Equals(desiredElem))
                {
                    linkedList.Insert(i, newElem);
                    i++;
                }
            }
        }

        /// <summary>
        /// 9. Функция дописывает к списку L список E.
        /// Оба списка содержат целые числа. В основной программе считать их из файла.
        /// </summary>

        public static void Add<T>(this LinkedListStruct<T> linkedList1, LinkedListStruct<T> linkedList2) where T : IComparable<T>
        {
            for (int i = 0; i < linkedList2.Count(); i++)
            {
                linkedList1.Add(linkedList2.ElementAt(i));
            }
        }

        /// <summary>
        /// 10. Функция разбивает список целых чисел на два списка по первому вхождению заданного числа.
        /// Если этого числа в списке нет, второй список будет пустым, а первый не изменится.
        /// </summary>

        public static LinkedListStruct<T> Split<T>(this LinkedListStruct<T> linkedList, T elem) where T : IComparable<T>
        {
            bool flag = false;
            LinkedListStruct<T> returnLinkedList = new LinkedListStruct<T>();
            for (int i = 0; i < linkedList.Count(); i++)
            {
                if (linkedList.ElementAt(i).Equals(elem))
                {
                    flag = true;
                }
                if (flag)
                {
                    returnLinkedList.Add(linkedList.ElementAt(i));
                    linkedList.RemoveAt(i);
                    i--;
                }
            }

            return returnLinkedList;
        }

        /// <summary>
        /// 11. Функция удваивает список, т.е. приписывает в конец списка себя самого.
        /// </summary>

        public static void Double<T>(this LinkedListStruct<T> linkedList) where T : IComparable<T>
        {
            int lenght = linkedList.Count();
            for (int i = 0; i < lenght; i++)
            {
                linkedList.Add(linkedList.ElementAt(i));
            }
        }

        /// <summary>
        /// 12. Функция меняет местами два элемента списка, заданные пользователем
        /// </summary>
        /// 
        public static void Flip<T>(this LinkedListStruct<T> linkedList, T elem1, T elem2) where T : IComparable<T>
        {
            int ind1 = linkedList.IndexOf(elem1);
            int ind2 = linkedList.IndexOf(elem2);
            linkedList.RemoveAt(ind1);
            linkedList.Insert(ind1, elem2);
            linkedList.Insert(ind2, elem1);
            linkedList.RemoveAt(ind2 + 1);
        }
    }
}