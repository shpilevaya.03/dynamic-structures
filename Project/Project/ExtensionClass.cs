﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public static class StackExtension
    {
        public static void Call<T>(this Stack<T> stack, int id, bool print, T? elem = default)
        {
            switch (id)
            {
                case 1:
                    if (elem is null) throw new ArgumentNullException();
                    stack.Push(elem);     if (print) stack.Print();                         break;
                case 2: stack.Pop();      if (print) stack.Print();                         break;
                case 3: if (print) Console.WriteLine(stack.Peek()); else stack.Peek();      break;
                case 4: if (print) Console.WriteLine(stack.Count == 0);                     break;
                case 5: if (print) stack.Print();                                           break;
                default: throw new Exception("Такой команды не существует!");
            }
        }

        public static void Print<T>(this Stack<T> stack)
        {
            T[] copyStack = new T[stack.Count];
            stack.CopyTo(copyStack, 0);

            for (int i = 0; i < copyStack.Length; i++)
            {
                Console.Write($"{copyStack[i]} ");
            }

            Console.WriteLine();
        }
    }

    public static class QueueExtension
    {
        public static void Call<T>(this Queue<T> queue, int id, bool print, T elem = default(T))
        {
            switch (id)
            {
                case 1:
                    if (elem is null) throw new ArgumentNullException();
                    queue.Enqueue(elem); if (print) queue.Print(); break;
                case 2: queue.Dequeue(); if (print) queue.Print(); break;
                case 3: if (print) Console.WriteLine(queue.Peek()); else queue.Peek(); break;
                case 4: if (print) Console.WriteLine(queue.Count == 0); break;
                case 5: if (print) queue.Print(); break;
                default: throw new Exception("Такой команды не существует!");
            }
        }

        public static void Print<T>(this Queue<T> queue)
        {
            T[] copyStack = new T[queue.Count];
            queue.CopyTo(copyStack, 0);

            for (int i = 0; i < copyStack.Length; i++)
            {
                Console.Write($"{copyStack[i]} ");
            }

            Console.WriteLine();
        }
    }
}
