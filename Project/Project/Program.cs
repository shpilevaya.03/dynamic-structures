﻿using System;
using System.Collections.Generic;
using Project.Core.Structures;
using Project.Core;
using Project.ConsoleUI;

namespace Project
{
    public class Program
    {
        public static void Main()
        {
            var fileWriter = new FileWriter();
            var fileReader = new FileReader();

            Console.CursorVisible = false;
            MainMenu menu = new MainMenu();
            bool exit = false;

            do
            {
                menu.Draw();

                ConsoleKeyInfo key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.DownArrow:
                        menu.Next();
                        break;
                    case ConsoleKey.UpArrow:
                        menu.Prev();
                        break;
                    case ConsoleKey.Enter:
                        exit = menu.Select();
                        break;
                }
            }
            while (!exit);

            //fileWriter.GenerateFile(DataWorker.Generate(5, 1000000));
            //fileWriter.GenerateInfixFile(100);

            //string str = "3 4 1,56 1,7 1,cat 2 5 4";

            //StructureWorker.ProcessCommand(new StackStruct<string>(), "3 4 1,56 1,7 1,cat 2 5 4".Split(' ', StringSplitOptions.RemoveEmptyEntries));


            //str = "7 8 + 3 2 + 5 8 * / / / * 4 6 9 -";
            //StructureWorker.CalculateCommands(str);

            //str = "2 + 4 / 5 * 5 - 3 ^ 5 ^ 4";
            //var strSplit = DataWorker.InfixToPostfix(fileReader.ReadFile(), new OperationsRepo());
            //StructureWorker.CalculateCommands(DataWorker.InfixToPostfix
            //    (fileReader.ReadFile(), new OperationsRepo())
            //    .Split(' ', StringSplitOptions.RemoveEmptyEntries)
            //    );
            //Console.WriteLine(strSplit);
            //StructureWorker.CalculateCommands(strSplit.Split(' ', StringSplitOptions.RemoveEmptyEntries));

            //Console.WriteLine($"{stack.GetType().Name}:\n" + new string('-', 10));

            //Console.WriteLine($"\n{queue.GetType().Name}\n" + new string('-', 10));
            //StructureWorker.ProcessCommand(queue, file.ReadFile());
        }
    }
}