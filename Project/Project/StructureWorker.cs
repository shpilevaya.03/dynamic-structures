﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Core;
using Project.Core.Structures;

namespace Project
{
    public static class StructureWorker
    {
        static FileWriter fileWriter = new FileWriter();

        public static void ProcessCommand(Stack<string> stack, string[] commands, bool print)
        {
            for (int i = 0; i < commands.Length; i++)
            {
                try
                {
                    if (int.TryParse(commands[i], out int id)) stack.Call(id, print);
                    else
                    {
                        var strSplit = commands[i].Split(',');
                        stack.Call(int.Parse(strSplit[0]), print, strSplit[1]);
                    }
                }
                catch (Exception e) { if(print) Console.WriteLine(e.Message); }
            }
        }

        public static void ProcessCommand(Queue<string> queue, string[] commands, bool print)
        {
            for (int i = 0; i < commands.Length; i++)
            {
                try
                {
                    if (int.TryParse(commands[i], out int id)) queue.Call(id, print);
                    else
                    {
                        var strSplit = commands[i].Split(',');
                        queue.Call(int.Parse(strSplit[0]), print, strSplit[1]);
                    }
                }
                catch (Exception e) { if (print) Console.WriteLine(e.Message); }
            }
        }

        public static void ProcessCommand(DynamicStructure<string> dynamicStructure, string[] commands, bool print)
        {
            for (int i = 0; i < commands.Length; i++)
            {
                try
                {
                    if (int.TryParse(commands[i], out int id)) dynamicStructure.Call(id, print);
                    else
                    {
                        var strSplit = commands[i].Split(',');
                        dynamicStructure.Call(int.Parse(strSplit[0]), print, strSplit[1]);
                    }
                }
                catch (Exception e) { if (print) Console.WriteLine(e.Message); }
            }
        }

        public static void CalculateCommands(string[] strSplit)
        {
            StackStruct<double> stackNums = new StackStruct<double>();
            StackStruct<Operation> stackOperations = new StackStruct<Operation>();
            List<Operation> operations = new OperationsRepo().GetOperations();

            for (int i = 0; i < strSplit.Length; i++)
            {
                if (double.TryParse(strSplit[i], out double num)) stackNums.Push(num);
                else
                {
                    stackOperations.Push(operations.First(op => op.Name == strSplit[i]));

                    var op = stackOperations.Pop();
                    if (op.hasTwoOperands)
                    {
                        var rigth = stackNums.Pop(); var left = stackNums.Pop();
                        stackNums.Push(op.Calculate(left, rigth));
                    }

                    else stackNums.Push(op.Calculate(stackNums.Pop()));
                    Console.WriteLine(stackNums.Top());
                }
            }
        }

        public static double MeasureExecutionTime(string[] arr, DynamicStructure<string> structure)
        {
            long freq = Stopwatch.Frequency; //частота таймера
            Stopwatch stopwatch = new Stopwatch();
            var results = new List<double>();

            for (int i = 0; i < 5; i++)
            {
                structure.Clear();
                stopwatch.Reset();
                stopwatch.Start();
                ProcessCommand(structure, arr, false);
                stopwatch.Stop();
                double sec = (double)stopwatch.ElapsedTicks / freq; //переводим такты в секунды
                Console.WriteLine($"Время выполнения в тактах \t{stopwatch.ElapsedTicks}\r\nВремя в секундах \t\t{sec}\r\n");
                results.Add(sec);
            }

            results.Remove(results.Max());

            Console.WriteLine($"Среднее время выполнения алгоритма: {results.Sum() / results.Count()} с\r\n" +
                new string('-', 20) + "\r\n");

            return results.Sum() / results.Count();
        }
        public static double MeasureExecutionTime(string[] arr, Stack<string> structure)
        {
            long freq = Stopwatch.Frequency; //частота таймера
            Stopwatch stopwatch = new Stopwatch();
            var results = new List<double>();

            for (int i = 0; i < 5; i++)
            {
                structure.Clear();
                stopwatch.Reset();
                stopwatch.Start();
                ProcessCommand(structure, arr, false);
                stopwatch.Stop();
                double sec = (double)stopwatch.ElapsedTicks / freq; //переводим такты в секунды
                Console.WriteLine($"Время выполнения в тактах \t{stopwatch.ElapsedTicks}\r\nВремя в секундах \t\t{sec}\r\n");
                results.Add(sec);
            }

            results.Remove(results.Max());

            Console.WriteLine($"Среднее время выполнения алгоритма: {results.Sum() / results.Count()} с\r\n" +
                new string('-', 20) + "\r\n");

            return results.Sum() / results.Count();
        }
        public static double MeasureExecutionTime(string[] arr, Queue<string> structure)
        {
            long freq = Stopwatch.Frequency; //частота таймера
            Stopwatch stopwatch = new Stopwatch();
            var results = new List<double>();

            for (int i = 0; i < 5; i++)
            {
                structure.Clear();
                stopwatch.Reset();
                stopwatch.Start();
                ProcessCommand(structure, arr, false);
                stopwatch.Stop();
                double sec = (double)stopwatch.ElapsedTicks / freq; //переводим такты в секунды
                Console.WriteLine($"Время выполнения в тактах \t{stopwatch.ElapsedTicks}\r\nВремя в секундах \t\t{sec}\r\n");
                results.Add(sec);
            }

            results.Remove(results.Max());

            Console.WriteLine($"Среднее время выполнения алгоритма: {results.Sum() / results.Count()} с\r\n" +
                new string('-', 20) + "\r\n");

            return results.Sum() / results.Count();
        }
        
        public static long MeasureExecutionMemory(string[] arr, Queue<string> structure)
        {
            var results = new List<long>();

            for (int i = 0; i < 5; i++)
            {
                structure.Clear();
                ProcessCommand(structure, arr, false);
                long memory = Process.GetCurrentProcess().WorkingSet64;
                Console.WriteLine($"Количество используемой памяти: \t{memory}\n");
                results.Add(memory);
            }

            results.Remove(results.Max());

            Console.WriteLine($"Среднее количество используемой памяти: {results.Sum() / results.Count()} байт\r\n" +
                new string('-', 20) + "\r\n");

            return results.Sum() / results.Count();
        }

        public static long MeasureExecutionMemory(string[] arr, Stack<string> structure)
        {
            var results = new List<long>();

            for (int i = 0; i < 5; i++)
            {
                structure.Clear();
                ProcessCommand(structure, arr, false);
                long memory = Process.GetCurrentProcess().WorkingSet64;
                Console.WriteLine($"Количество используемой памяти: \t{memory}\n");
                results.Add(memory);
            }

            results.Remove(results.Max());

            Console.WriteLine($"Среднее количество используемой памяти: {results.Sum() / results.Count()} байт\r\n" +
                new string('-', 20) + "\r\n");

            return results.Sum() / results.Count();
        }

        public static long MeasureExecutionMemory(string[] arr, DynamicStructure<string> structure)
        {
            var results = new List<long>();

            for (int i = 0; i < 5; i++)
            {
                structure.Clear();
                ProcessCommand(structure, arr, false);
                long memory = Process.GetCurrentProcess().WorkingSet64;
                Console.WriteLine($"Количество используемой памяти: \t{memory}\n");
                results.Add(memory);
            }

            results.Remove(results.Max());

            Console.WriteLine($"Среднее количество используемой памяти: {results.Sum() / results.Count()} байт\r\n" +
                new string('-', 20) + "\r\n");

            return results.Sum() / results.Count();
        }

        public static void RepeatMeasure(List<string> commands, DynamicStructure<string> structure, DataPath writePath, int step, int stepCount)
        {
            fileWriter.ChangePath(writePath);
            var sb = new StringBuilder();

            for (int i = 0; i < stepCount; i++)
            {
                structure.Clear();
                var arr = commands.GetRange(0, step * (i + 1)).ToArray();
                Console.WriteLine($"Количество элементов: {arr.Length}");
                sb.Append(arr.Length + " ");
                if (writePath == DataPath.time) sb.Append(MeasureExecutionTime(arr, structure));
                if (writePath == DataPath.memory) sb.Append(MeasureExecutionMemory(arr, structure));
                sb.AppendLine();
            }

            fileWriter.GenerateFile(sb.ToString());
        }

        public static void RepeatMeasure(List<string> commands, Stack<string> structure, DataPath writePath, int step, int stepCount)
        {
            fileWriter.ChangePath(writePath);
            var sb = new StringBuilder();

            for (int i = 0; i < stepCount; i++)
            {
                structure.Clear();
                var arr = commands.GetRange(0, step * (i + 1)).ToArray();
                Console.WriteLine($"Количество элементов: {arr.Length}");
                sb.Append(arr.Length + " ");
                if(writePath == DataPath.time) sb.Append(MeasureExecutionTime(arr, structure));
                if(writePath == DataPath.memory) sb.Append(MeasureExecutionMemory(arr, structure));
                sb.AppendLine();
            }

            fileWriter.GenerateFile(sb.ToString());
        }

        public static void RepeatMeasure(List<string> commands, Queue<string> structure, DataPath writePath, int step, int stepCount)
        {
            fileWriter.ChangePath(writePath);
            var sb = new StringBuilder();

            for (int i = 0; i < stepCount; i++)
            {
                structure.Clear();
                var arr = commands.GetRange(0, step * (i + 1)).ToArray();
                Console.WriteLine($"Количество элементов: {arr.Length}");
                sb.Append(arr.Length + " ");
                if (writePath == DataPath.time) sb.Append(MeasureExecutionTime(arr, structure));
                if (writePath == DataPath.memory) sb.Append(MeasureExecutionMemory(arr, structure));
                sb.AppendLine();
            }

            fileWriter.GenerateFile(sb.ToString());
        }

        public static Queue<T> QueueExample<T>(Queue<T> firstQueue, Queue<T> secondQueue)
        {
            Queue<T> result = new Queue<T>();
            int index = 0;
            while (index <= secondQueue.Count)
            {
                if (index % 2 != 0) result.Enqueue(firstQueue.ElementAt(index));
                else result.Enqueue(secondQueue.ElementAt(index));
                index++;
            }
            return result;
        }

        public static double StackExample(string str)
        {
            StackStruct<double> stack = new StackStruct<double>();
            int i = 0; double result = 0;
            string[] arr = str.Split(" ");
            while (arr.Length > i)
            {
                double num;
                bool isNum = double.TryParse(arr[i], out num);
                if (isNum) stack.Push(num);
                else
                {
                    var operationsRepo = new OperationsRepo();
                    List<Operation> operations = operationsRepo.GetOperations();
                    Operation op = operations.First(x => x.Name == arr[i]);
                    if (op.hasTwoOperands)
                    {
                        double operand1 = stack.Pop();
                        double operand2 = stack.Pop();
                        stack.Push(op.Calculate(operand2, operand1));
                    }
                    else
                    {
                        double operand1 = stack.Pop();
                        stack.Push(op.Calculate(operand1));
                    }
                }
                i++;
            }
            return stack.Pop();
        }

        public static List<T> ListExample<T>(List<T> firstList, List<T> secondList)
        {
            List<T> result = new List<T>();
            foreach (T item in firstList)
            {
                if (secondList.Contains(item))
                {
                    result.Add(item);
                }
            }
            return result;
        }

        public static int TreeExample(TreeStruct root)
        {
            if (root == null) return 0;
            return root.Value + TreeExample(root.Left) + TreeExample(root.Right);
        }
    }
}
