﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Core;
using Project.Core.Structures;

namespace Project.ConsoleUI
{
    public abstract class BaseMenu
    {
        protected enum ExeType
        {
            ListMethods,
            StandartStackMeasureTime,
            StackMeasureTime,
            StandartQueueMeasureTime,
            QueueMeasureTime,
            StackProcessCommand,
            QueueProcessCommand,
            StandartStackMeasureMemory,
            StackMeasureMemory,
            StandartQueueMeasureMemory,
            QueueMeasureMemory,
            PostfixNotation,
            PostfixNotationFromFile,
            Exit
        }

        protected enum ListMethodType
        {
            Reverse,
            FlipLastOrFirst,
            CountDistinct,
            RemoveSecondClone,
            InsertIntoAfterFirstOccurrence,
            OrderedInsertion,
            RemoveElems,
            InsertInto,
            AddLinkedList,
            SplitAfterFirstOccurrence,
            Double,
            Flip,
            Print,
            Add,
            Exit
        }

        protected class MenuItem
        {
            public string Text;
            public bool IsSelected;
            public DataPath? ReadPath;
            public DataPath? WritePath;
            public ExeType Type;
            public ListMethodType? ListType;
        }

        protected abstract List<MenuItem> Items { get; }
        List<MenuItem> _items = new List<MenuItem>();
        protected LinkedListStruct<string> linkedListStruct = new LinkedListStruct<string>();
        protected Stack<string> baseStask = new Stack<string>();
        protected StackStruct<string> dynamicStack = new StackStruct<string>();
        protected Queue<string> baseQueue = new Queue<string>();
        protected QueueStruct<string> dynamicQueue = new QueueStruct<string>();

        public BaseMenu()
        {
            _items = Items.ToList();
        }

        public virtual void Draw()
        {
            ConsoleHelper.ClearScreen();

            foreach (var menuItem in _items)
            {
                Console.BackgroundColor = menuItem.IsSelected
                    ? Color.HighlightColor
                    : Color.BaseColor;

                Console.WriteLine("| " + menuItem.Text);
            }

            Console.BackgroundColor = Color.BaseColor;
        }
        public void SubMenu()
        {
            bool back = false;

            do
            {
                Draw();

                ConsoleKeyInfo key = Console.ReadKey(true);

                switch (key.Key)
                {
                    case ConsoleKey.DownArrow:
                        Next();
                        break;
                    case ConsoleKey.UpArrow:
                        Prev();
                        break;
                    case ConsoleKey.Enter:
                        var selectedItem = _items.First(x => x.IsSelected);

                        back = selectedItem.ListType == ListMethodType.Exit;

                        if (!back)
                        {
                            ConsoleHelper.ClearScreen();

                            try {
                                CheckListMethodType((ListMethodType)selectedItem.ListType);
                            }
                            catch(Exception e) { Console.WriteLine(e.Message); }

                            Console.WriteLine();
                            ConsoleHelper.PrintHighlightedText("Для выхода нажмите любую клавишу");
                            Console.ReadKey();
                        }
                        break;
                }
            }
            while (!back);
        }


        public void Next()
        {
            var selectedItem = _items.First(x => x.IsSelected);
            var selectedInd = _items.IndexOf(selectedItem);
            selectedItem.IsSelected = false;

            selectedInd = selectedInd == _items.Count - 1
                ? 0
                : ++selectedInd;

            _items[selectedInd].IsSelected = true;
        }

        public void Prev()
        {
            var selectedItem = _items.First(x => x.IsSelected);

            var selectedInd = _items.IndexOf(selectedItem);

            selectedItem.IsSelected = false;

            selectedInd = selectedInd <= 0
                ? _items.Count - 1
                : --selectedInd;

            _items[selectedInd].IsSelected = true;
        }

        public bool Select()
        {
            var selectedItem = _items.First(x => x.IsSelected);

            if(selectedItem.Type == ExeType.Exit) return true;

            ConsoleHelper.ClearScreen();

            CheckType(selectedItem.Type, selectedItem.ListType, selectedItem.ReadPath, selectedItem.WritePath);

            Console.WriteLine();
            if (selectedItem.Type is not ExeType.ListMethods)
            {
                ConsoleHelper.PrintHighlightedText("Для выхода нажмите любую клавишу");
                Console.ReadKey();
            }

            return false;
        }

        private void CheckType(ExeType type, ListMethodType? listMethodType, DataPath? readPath, DataPath? writePath)
        {
            int count = 1000;
            int stepCount = 100;

            FileReader fileReader = new FileReader(readPath);
            FileWriter fileWriter = new FileWriter(writePath);

            switch(type)
            {
                case ExeType.ListMethods:
                    ListMenu listMenu = new ListMenu();
                    ConsoleHelper.PrintHighlightedText("Введите элементы связного списка через пробел: ");
                    var strings = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    foreach (var str in strings)
                    {
                        listMenu.linkedListStruct.Add(str);
                    }
                    Thread.Sleep(1000);
                    listMenu.SubMenu();
                    break;
                case ExeType.StandartStackMeasureTime:
                    StructureWorker.RepeatMeasure(fileReader.ReadFile().ToList(), new Stack<string>(), (DataPath)writePath, count, stepCount);
                    break;
                case ExeType.StackMeasureTime:
                    StructureWorker.RepeatMeasure(fileReader.ReadFile().ToList(), new StackStruct<string>(), (DataPath)writePath, count, stepCount);
                    break;
                case ExeType.StandartQueueMeasureTime:
                    StructureWorker.RepeatMeasure(fileReader.ReadFile().ToList(), new Queue<string>(), (DataPath)writePath, count, stepCount);
                    break;
                case ExeType.QueueMeasureTime:
                    StructureWorker.RepeatMeasure(fileReader.ReadFile().ToList(), new QueueStruct<string>(), (DataPath)writePath, count, stepCount);
                    break;
                case ExeType.StackProcessCommand:
                    ConsoleHelper.PrintHighlightedText("Введите строку, состоящую из команд: ");
                    Console.WriteLine("Операции:\n1 - вставка,\n2 - удаление,\n3 – просмотр начала очереди,\n4 – проверка на пустоту,\n5 - печать.\n");
                    StructureWorker.ProcessCommand(new StackStruct<string>(), Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries), true);
                    break;
                case ExeType.QueueProcessCommand:
                    ConsoleHelper.PrintHighlightedText("Введите строку, состоящую из команд: ");
                    Console.WriteLine("Операции:\n1 - вставка,\n2 - удаление,\n3 – просмотр начала очереди,\n4 – проверка на пустоту,\n5 - печать.\n");
                    StructureWorker.ProcessCommand(new QueueStruct<string>(), Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries), true);
                    break;
                case ExeType.StandartStackMeasureMemory:
                    StructureWorker.RepeatMeasure(fileReader.ReadFile().ToList(), new QueueStruct<string>(), (DataPath)writePath, count, stepCount);
                    break;
                case ExeType.StackMeasureMemory:
                    StructureWorker.RepeatMeasure(fileReader.ReadFile().ToList(), new QueueStruct<string>(), (DataPath)writePath, count, stepCount);
                    break;
                case ExeType.StandartQueueMeasureMemory:
                    StructureWorker.RepeatMeasure(fileReader.ReadFile().ToList(), new QueueStruct<string>(), (DataPath)writePath, count, stepCount);
                    break;
                case ExeType.QueueMeasureMemory:
                    StructureWorker.RepeatMeasure(fileReader.ReadFile().ToList(), new QueueStruct<string>(), (DataPath)writePath, count, stepCount);
                    break;
                case ExeType.PostfixNotation:
                    ConsoleHelper.PrintHighlightedText("Введите формулу в инфиксной нотации, разделяя каждый элемент пробелом: ");

                    var infix = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    Console.WriteLine(DataWorker.InfixToPostfix(infix, new OperationsRepo()));

                    try { StructureWorker.CalculateCommands(DataWorker.InfixToPostfix
                        (infix, new OperationsRepo())
                        .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                        );
                    }
                    catch (Exception e) { Console.WriteLine(e.Message); }
                    break;
                case ExeType.PostfixNotationFromFile:
                    //fileWriter.GenerateInfixFile(1000000);

                    string postfix = DataWorker.InfixToPostfix(fileReader.ReadFile(), new OperationsRepo());
                    Console.WriteLine(postfix);
                    Console.WriteLine();
                    StructureWorker.CalculateCommands(postfix
                        .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                        );
                    break;
            }
        }

        private void CheckListMethodType(ListMethodType type)
        {
            Console.WriteLine($"Актуальный список: {linkedListStruct.ToString()}");

            switch (type)
            {
                case ListMethodType.Add:
                    ConsoleHelper.PrintHighlightedText("Введите значения через пробел: ");
                    string[] strings = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    foreach (var str in strings)
                    {
                        linkedListStruct.Add(str);
                    }
                    break;
                case ListMethodType.Reverse:
                    linkedListStruct.Reverse();
                    Console.WriteLine(linkedListStruct.ToString());
                    return;
                case ListMethodType.FlipLastOrFirst:
                    ConsoleHelper.PrintHighlightedText("Хотите, чтобы был перенесён последний элемент списка в начало? (y/n)");
                    linkedListStruct.Flip(ConsoleHelper.GetYesOrNoAnswer());
                    break;
                case ListMethodType.CountDistinct:
                    Console.WriteLine(linkedListStruct.CountDistinct());
                    return;
                case ListMethodType.RemoveSecondClone:
                    linkedListStruct.RemoveSecondClone();
                    break;
                case ListMethodType.InsertIntoAfterFirstOccurrence:
                    ConsoleHelper.PrintHighlightedText("Напишите значение элемента, вслед за первым вхождением которого нужно вставить список в самого себя: ");
                    linkedListStruct.InsertInto(Console.ReadLine());
                    break;
                case ListMethodType.OrderedInsertion:
                    ConsoleHelper.PrintHighlightedText("Введите новый элемент, который хотите вставить в упорядоченный список: ");
                    linkedListStruct.OrderedInsertion(Console.ReadLine());
                    break;
                case ListMethodType.RemoveElems:
                    ConsoleHelper.PrintHighlightedText("Введите элемент, все вхождения которого хотите удалить: ");
                    linkedListStruct.RemoveAll(Console.ReadLine());
                    break;
                case ListMethodType.InsertInto:
                    ConsoleHelper.PrintHighlightedText("Введите элемент, перед первым вхождением которого должен быть вставлен новый элемент: ");
                    string desiredElem =    Console.ReadLine();
                    ConsoleHelper.PrintHighlightedText("Введите новый элемент, который должен быть вставлен в список: ");
                    string newElem =        Console.ReadLine();
                    linkedListStruct.InsertInto(newElem, desiredElem);
                    break;
                case ListMethodType.AddLinkedList:
                    LinkedListStruct<string> linkedList = new LinkedListStruct<string>();
                    ConsoleHelper.PrintHighlightedText("Введите элементы списка, который хотите добавить (через пробел): ");
                    strings = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    foreach (var str in strings)
                    {
                        linkedList.Add(str);
                    }
                    linkedListStruct.Add(linkedList);
                    break;
                case ListMethodType.SplitAfterFirstOccurrence:
                    ConsoleHelper.PrintHighlightedText("Введите элемент, после первого вхождения которого хотите разбить список на два: ");
                    var list = linkedListStruct.Split(Console.ReadLine());
                    Console.WriteLine($"Первый список: {linkedListStruct.ToString()}\nВторой список: {list.ToString()}");
                    return;
                case ListMethodType.Double:
                    linkedListStruct.Double();
                    break;
                case ListMethodType.Flip:
                    ConsoleHelper.PrintHighlightedText("Введите 2 элемента, которые хотите поменять местами в списке (через пробел): ");
                    strings = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    linkedListStruct.Flip(strings[0], strings[1]);
                    break;
                case ListMethodType.Print:
                    return;
            }

            Console.WriteLine(linkedListStruct.ToString());
        }
    }

    class ListMenu : BaseMenu
    {
        protected override List<MenuItem> Items => new List<MenuItem>
        {
            new MenuItem() { Text = "Вывести список на экран", ListType = ListMethodType.Print, IsSelected = true },
            new MenuItem() { Text = "Добавить элементы", ListType = ListMethodType.Add },
            new MenuItem() { Text = "Функция, которая переворачивает список L, т.е. изменяет ссылки в этом списке так, чтобы его элементы оказались расположенными в обратном порядке.", ListType = ListMethodType.Reverse  },
            new MenuItem() { Text = "Функция, которая переносит в начало (в конец) непустого списка L его последний (первый) элемент.", ListType = ListMethodType.FlipLastOrFirst },
            new MenuItem() { Text = "Функция, которая определяет количество различных элементов списка, содержащего целые числа.", ListType = ListMethodType.CountDistinct },
            new MenuItem() { Text = "Функция, которая удаляет из списка L второй из двух одинаковых элементов.", ListType = ListMethodType.RemoveSecondClone },
            new MenuItem() { Text = "Функция вставки списка самого в себя вслед за первым вхождением числа х.", ListType = ListMethodType.InsertIntoAfterFirstOccurrence },
            new MenuItem() { Text = "Функция, которая вставляет в непустой список L, элементы которого упорядочены по не убыванию, новый элемент Е так, чтобы сохранилась упорядоченность.", ListType = ListMethodType.OrderedInsertion },
            new MenuItem() { Text = "Функция, которая удаляет из списка L все элементы Е, если таковые имеются. ", ListType = ListMethodType.RemoveElems },
            new MenuItem() { Text = "Функция, которая вставляет в список L новый элемент F перед первым вхождением элемента Е, если Е входит в L.", ListType = ListMethodType.InsertInto },
            new MenuItem() { Text = "Функция дописывает к списку L список E. Оба списка содержат целые числа. В основной программе считать их из файла.", ListType = ListMethodType.AddLinkedList },
            new MenuItem() { Text = "Функция разбивает список целых чисел на два списка по первому вхождению заданного числа. Если этого числа в списке нет, второй список будет пустым, а первый не изменится.", ListType = ListMethodType.SplitAfterFirstOccurrence },
            new MenuItem() { Text = "Функция удваивает список, т.е. приписывает в конец списка себя самого.", ListType = ListMethodType.Double },
            new MenuItem() { Text = "Функция меняет местами два элемента списка, заданные пользователем", ListType = ListMethodType.Flip },
            new MenuItem() { Text = "Выход", ListType = ListMethodType.Exit }
        };
    }
}
