﻿using System;
using System.Collections.Generic;
using System.Text;
using Project.Core;

namespace Project.ConsoleUI
{
    class MainMenu : BaseMenu
    {
        protected override List<MenuItem> Items => new List<MenuItem>
        {
            new MenuItem() { Text = "Методы для работы со списком", Type = ExeType.ListMethods, IsSelected = true },
            new MenuItem() { Text = "Записать в файл время обработки команд для стандартного стека", Type = ExeType.StandartStackMeasureTime, ReadPath = DataPath.commands, WritePath = DataPath.time },
            new MenuItem() { Text = "Записать в файл время обработки команд для стека", Type = ExeType.StackMeasureTime, ReadPath = DataPath.commands, WritePath = DataPath.time },
            new MenuItem() { Text = "Записать в файл время обработки команд для стандартной очереди", Type = ExeType.StandartQueueMeasureTime, ReadPath = DataPath.commands, WritePath = DataPath.time },
            new MenuItem() { Text = "Записать в файл время обработки команд для очереди", Type = ExeType.QueueMeasureTime, ReadPath = DataPath.commands, WritePath = DataPath.time },
            new MenuItem() { Text = "Вывести на экран процесс обработки команд стеком", Type = ExeType.StackProcessCommand },
            new MenuItem() { Text = "Вывести на экран процесс обработки команд очередью", Type = ExeType.QueueProcessCommand },
            new MenuItem() { Text = "Записать в файл траты по памяти в процессе обработки команд для стандартного стека", Type = ExeType.StandartStackMeasureMemory, ReadPath = DataPath.commands, WritePath = DataPath.memory },
            new MenuItem() { Text = "Записать в файл траты по памяти в процессе обработки команд для стека", Type = ExeType.StackMeasureMemory , ReadPath = DataPath.commands, WritePath = DataPath.memory },
            new MenuItem() { Text = "Записать в файл траты по памяти в процессе обработки команд для стандартной очереди", Type = ExeType.StandartQueueMeasureMemory , ReadPath = DataPath.commands, WritePath = DataPath.memory},
            new MenuItem() { Text = "Записать в файл траты по памяти в процессе обработки команд для очереди", Type = ExeType.QueueMeasureMemory, ReadPath = DataPath.commands, WritePath = DataPath.memory },
            new MenuItem() { Text = "Привести строку из инфикса в постфикс и обработать её", Type = ExeType.PostfixNotation },
            new MenuItem() { Text = "Обработать польскую нотацию из файла", Type = ExeType.PostfixNotationFromFile, ReadPath = DataPath.infix, WritePath = DataPath.infix },
            new MenuItem() { Text = "Выход", Type = ExeType.Exit }
        };
    }
}

